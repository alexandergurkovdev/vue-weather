import Vue from 'vue';
import App from './App.vue';
import store from './store';
import dateFilter from '@/filters/date.filter';
import 'vue-slick-carousel/dist/vue-slick-carousel.css';
import 'vue-slick-carousel/dist/vue-slick-carousel-theme.css';
import './css/app.css';

Vue.config.productionTip = false;
Vue.filter('date', dateFilter);

new Vue({
  store,
  render: h => h(App)
}).$mount('#app');
