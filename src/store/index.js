import Vue from 'vue';
import Vuex from 'vuex';

const API_KEY = 'cffcca366b7443fbabae5b8269880f24';

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    loading: false,
    error: null,
    currentWeather: null,
    forecast: null
  },
  mutations: {
    setLoading(state, loading) {
      state.loading = loading;
    },
    setError(state, error) {
      state.error = error;
    },
    setCurrentWeather(state, weather) {
      state.currentWeather = weather;
    },
    setForecast(state, forecast) {
      state.forecast = forecast;
    }
  },
  actions: {
    async fetchCurrentWeather({ commit }, coordinates) {
      commit('setLoading', true);
      await fetch(
        `https://api.weatherbit.io/v2.0/current?lat=${coordinates.latitude}&lon=${coordinates.longitude}&key=${API_KEY}&`
      )
        .then(res => res.json())
        .then(
          res => {
            commit('setCurrentWeather', res.data[0]);
          },
          error => {
            commit('setError', error);
          }
        );
      commit('setLoading', false);
    },
    async fetchForecast({ commit }, coordinates) {
      commit('setLoading', true);
      await fetch(
        `https://api.weatherbit.io/v2.0/forecast/daily?lat=${coordinates.latitude}&lon=${coordinates.longitude}&key=${API_KEY}&`
      )
        .then(res => res.json())
        .then(
          res => {
            commit('setForecast', res.data);
          },
          error => {
            commit('setError', error);
          }
        );
      commit('setLoading', false);
    }
  },
  getters: {
    loading(state) {
      return state.loading;
    },
    error(state) {
      return state.error;
    },
    currentWeather(state) {
      return state.currentWeather;
    },
    forecast(state) {
      return state.forecast;
    }
  },
  modules: {}
});
