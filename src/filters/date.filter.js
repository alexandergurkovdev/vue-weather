export default function dateFilter(value) {
  return value.toLocaleString('en-US', { hour: 'numeric', minute: 'numeric', hour12: true });
}
